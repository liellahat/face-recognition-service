from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import numpy as np
from pymongo import MongoClient
from sorted_collection import SortedCollection
from bson import ObjectId
import json

app = Flask(__name__)
api = Api(app)

client = MongoClient('localhost:27017')

db = client.face_recognition
cache_to_publish = []  # Reduce db calls

post_parser = reqparse.RequestParser()
post_parser.add_argument('vector', type=list, location='json', required=True, help='need face vector')
post_parser.add_argument('name', type=str, location='json', required=True, help="name of vector's person")

get_parser = reqparse.RequestParser()
get_parser.add_argument('vector', type=list, location='json', required=True, help='need face vector')
get_parser.add_argument('amount', type=int, help='number of vectors to return')


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


def check_vector(vector):
    if len(vector) != 256:
        abort(400, message='vector must be of length 256')


def get_similar_vectors(vector, amount):

    best_matches = SortedCollection(key=lambda x: np.dot(vector, x['vector']))
    cursor = db.people.find({})

    for i in range(amount):
        doc = next(cursor, None)
        if doc:
            best_matches.insert(doc)

    thresh_dot_prod = np.dot(best_matches[0]['vector'], vector)  # smallest dotProduct is the threshold

    curr_doc = next(cursor, None)
    while curr_doc:
        curr_dot_prod = np.dot(vector, curr_doc['vector'])

        if curr_dot_prod > thresh_dot_prod:
            best_matches.insert(curr_doc)
            best_matches.remove(best_matches[0])
            thresh_dot_prod = np.dot(best_matches[0]['vector'], vector)
        curr_doc = next(cursor, None)

    return best_matches


class Service(Resource):

    def get(self):
        args = get_parser.parse_args()
        check_vector(args['vector'])
        amount = args.get('amount', 3)
        return {
            'status': 200,
            'result': json.dumps(get_similar_vectors(args['vector'], amount).get_items(), cls=JSONEncoder)
        }

    def post(self):
        args = post_parser.parse_args()
        #cache_to_publish.append({'name': args['name'], 'vector': args['vector']})
        check_vector(args['vector'])
        result = db.people.insert_one({'name': args['name'], 'vector': args['vector']})
        return {'status': 200, 'result_obj': str(result.inserted_id)}


api.add_resource(Service, '/service')

if __name__ == '__main__':
    app.run(debug=True)

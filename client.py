from requests import put, get, post
import random
import json
from pprint import pprint


def gen_vector():
    return [random.random() for _ in range(256)]


def get_closest_vector():
    vector = gen_vector()
    res = get('http://localhost:5000/service', json={'vector': vector, 'amount': 3})
    return json.loads(res.text)


def feed_db():
    input_vectors = [gen_vector() for _ in range(100)]
    for vector in input_vectors:
        res = post('http://localhost:5000/service', json={'name': 'george', 'vector': vector})
        print(res.text)


if __name__ == '__main__':
    # feed_db()
    docs = json.loads(get_closest_vector()['result'])
    pprint(docs)
